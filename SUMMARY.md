# Table of contents

* [StreamersEdge](README.md)

## FAQ

* [General](faq/account-creation.md)
* [Creating an Account](faq/creating-an-account.md)
* [Log In](faq/log-in.md)
* [Connecting Accounts](faq/linking-accounts.md)
* [Creating Challenges](faq/creating-challenges.md)
* [Making Conditional Donations](faq/making-donations.md)
* [Money](faq/money.md)
* [Reporting a User](faq/reporting-a-user.md)
* [Privacy](faq/privacy.md)
* [Customer Support](faq/customer-support.md)

## Help

* [Creating an Account](help/creating-an-account.md)
* [Logging In](help/logging-in/README.md)
  * [Forgot Password](help/logging-in/forgot-password.md)
* [Dashboard](help/dashboard/README.md)
  * [Left Menu](help/dashboard/left-menu.md)
  * [Right Menu](help/dashboard/right-menu.md)
* [Update Profile](help/update-profile.md)
* [Preferences](help/preferences.md)
* [Funding and Redeeming](help/payments-and-redemptions/README.md)
  * [Redeem Balance](help/payments-and-redemptions/redeem-money.md)
  * [Add Funds](help/payments-and-redemptions/add-funds.md)
* [Challenges](help/creating-a-challenge/README.md)
  * [Creating a Challenge](help/creating-a-challenge/creating-a-challenge.md)
  * [Challenge Details](help/creating-a-challenge/challenge-details.md)
  * [Challenge Grid](help/creating-a-challenge/challenge-grid.md)
* [Donations](help/donations.md)
* [Report a User](help/report-a-user.md)

## Payment Flow <a href="#payment" id="payment"></a>

* [High Level](payment/high-level.md)
* [Streamer](payment/streamer.md)
* [Viewer](payment/viewer.md)

## API

* [API Reference](api/summary.md)
