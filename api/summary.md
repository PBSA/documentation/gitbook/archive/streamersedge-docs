# API Reference

The StreamersEdge REST API allows mobile, web and desktop applications as well as other services like Twitch extension, Discord bots etc. to connect to it.&#x20;

The API follows the OpenAPI specifications.

**Functionalities provided by API end points include:**

* Provide game details
* Create the Challenge
* Join Challenge as a user
* Details of a challenge in progress
* An end point to be called to manually trigger fetching of the data
* Trigger donation
* Raise Dispute
* Disperse funds
* Dispute resolutions

### listChallenges

List all challenges.

```http
GET /listChallenges/:limit/:nextId/:search_param/:search_text
```

{% tabs %}
{% tab title="Parameters" %}
* **`limit`**: \[optional] How many items to return at one time (max 100) .
* **`nextId`**: \[optional] Id of the next challenge to be returned.
* **`search_param`**: \[optional] Parameter to be searched.
* **`search_text`**: \[optional] Text to be searched in the parameter values.
{% endtab %}

{% tab title="Response" %}
* **`200`**: A paged array of challenges.
* **`default`**: Unexpected error.
{% endtab %}

{% tab title="Example" %}
```csharp
_data.listChallenges(
    limit: integer, 
    nextId: integer, 
    search_param: string, 
    search_text: string).subscribe()
```
{% endtab %}
{% endtabs %}

### createChallenge

Create a challenge

```http
POST /createChallenge/:challenge
```

{% tabs %}
{% tab title="Parameters" %}
* **`challenge`**: Challenge to be created
{% endtab %}

{% tab title="Response" %}
* **`201`**: Null response.
* **`default`**: Unknown error.
{% endtab %}

{% tab title="Example" %}
```csharp
_data.createChallenge(challenge: object<Challenge>).subscribe()
```
{% endtab %}
{% endtabs %}

### &#x20;showChallengeById&#x20;

Get information for a specific challenge

```http
GET /showChallengeById/:challengeId
```

{% tabs %}
{% tab title="Parameters" %}
* **`challengeId`**: The id of the challenge to retrieve.
{% endtab %}

{% tab title="Response" %}
* **`200`**: Expected response to a valid request.
* **`default`**: unexpected error.
{% endtab %}

{% tab title="Example" %}
```csharp
_data.showChallengeById(challengeId: integer).subscribe()
```
{% endtab %}
{% endtabs %}

### listStreams

Get a list of all streams.

```http
GET /listStreams/:limit/:nextId/:search_param/:search_text
```

{% tabs %}
{% tab title="Parameters" %}
* **`limit`**: \[optional] How many items to return at one time (max 100) .
* **`nextId`**: \[optional] Id of the next stream to be returned.
* **`search_param`**: \[optional] Parameter to be searched.
* **`search_text`**: \[optional] Text to be searched in the parameter values.
{% endtab %}

{% tab title="Response" %}
* **`200`**: A paged array of streams.
* **`default`**: Unexpected error.
{% endtab %}

{% tab title="Example" %}
```csharp
_data.listStreams(
    limit: integer, 
    nextId: integer, 
    search_param: string, 
    search_text: string).subscribe()
```
{% endtab %}
{% endtabs %}

### addStream&#x20;

Add a newly created stream on twitch/youtube.

```http
POST /addStream/:stream
```

{% tabs %}
{% tab title="Parameters" %}
* **`stream`**: Stream to be added.
{% endtab %}

{% tab title="Response" %}
* **`201`**: Null response.
* **`default`**: Unknown error.
{% endtab %}

{% tab title="Example" %}
```csharp
_data.addStream(stream: object<Stream>).subscribe()
```
{% endtab %}
{% endtabs %}

### showStreamById

Get information for a specific stream .

```http
GET /showStreamById/:streamId
```

{% tabs %}
{% tab title="Parameters" %}
* **`streamId`**: The id of the stream to retrieve.
{% endtab %}

{% tab title="Response" %}
* **`200`**: Expected response to a valid request.
* **`default`**: unexpected error.
{% endtab %}

{% tab title="Example" %}
```csharp
_data.showStreamById(streamId: integer).subscribe()
```
{% endtab %}
{% endtabs %}

### listGames

Get a list of all the games data.

```http
GET /listGames/:limit/:nextId/:search_param/:search_text
```

{% tabs %}
{% tab title="Parameters" %}
* **`limit`**: \[optional] How many items to return at one time (max 100) .
* **`nextId`**: \[optional] Id of the next stream to be returned.
* **`search_param`**: \[optional] Parameter to be searched.
* **`search_text`**: \[optional] Text to be searched in the parameter values.
{% endtab %}

{% tab title="Response" %}
* **`200`**: A paged array of `game_data`.
* **`default`**: Unexpected error.
{% endtab %}

{% tab title="Example" %}
```csharp
_data.listGames(
    limit: integer, 
    nextId: integer, 
    search_param: string, 
    search_text: string).subscribe()
```
{% endtab %}
{% endtabs %}

### &#x20;addGameData

Add a newly retrieved game data from game API.

```http
POST /addGameData/:game_data
```

{% tabs %}
{% tab title="Parameters" %}
* **`game_data`**: Game data to be added.
{% endtab %}

{% tab title="Response" %}
* **`201`**: Null response.
* **`default`**: Unknown error.
{% endtab %}

{% tab title="Example" %}
```csharp
_data.addGameData(gamedata: object<Game_data>).subscribe()
```
{% endtab %}
{% endtabs %}

### showGameDataById

Add a newly retrieved game data from game API.

```http
GET /showGameDataById/:game_data_id
```

{% tabs %}
{% tab title="Parameters" %}
* **`game_data_id`**: The id of the game data to retrieve.
{% endtab %}

{% tab title="Response" %}
* **`200`**: Expected response to a valid request.
* **`default`**: unexpected error.
{% endtab %}

{% tab title="Example" %}
```csharp
_data.showGameDataById(game_data_id: integer).subscribe()
```
{% endtab %}
{% endtabs %}

### createUserProfile

Create or edit a user profile.

```http
POST /createUserProfile/:profile
```

{% tabs %}
{% tab title="Parameters" %}
* **`profile`**: User Profile to be created or edited.
{% endtab %}

{% tab title="Response" %}
* **`201`**: Null response.
* **`default`**: Unknown error.
{% endtab %}

{% tab title="Example" %}
```csharp
_data.addGameData(profile: object<Profile>).subscribe()
```
{% endtab %}
{% endtabs %}

### showProfileByUserId

Add a newly retrieved game data from game API.

```http
GET /showProfileByUserId/:user_id
```

{% tabs %}
{% tab title="Parameters" %}
* **`user_id`**: The id of the user whose profile has to be retrieved
{% endtab %}

{% tab title="Response" %}
* **`200`**: Expected response to a valid request.
* **`default`**: unexpected error.
{% endtab %}

{% tab title="Example" %}
```csharp
_data.showProfileByUserId(user_id: integer).subscribe()
```
{% endtab %}
{% endtabs %}
