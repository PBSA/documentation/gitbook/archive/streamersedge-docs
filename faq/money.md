# Money

### **How do I fund my account?**

To fund your account open the right menu section by clicking on the 👤icon at the right of the title bar, then select _Add Funds_. The only payment method supported at present is PayPal, so after you enter the amount to fund your account click the PayPal button and then follow your normal PayPal process to withdraw money.

### **How do I withdraw my money?**

To withdraw money from your account open the right menu section by clicking on the 👤icon at the right of the title bar, then select _Redeem Balance_. The only payment method supported at present is PayPal, so after you enter the amount to redeem from your account click the _Redeem_ button and then follow your normal PayPal process to add money.

### **What fees will I have to pay?**

When you win a challenge, there are no fees charged on the amount win.&#x20;

However, when you make a donation a nominal flat fee of 0.02 USD is charged and when you redeem money a fee of 5% is charged.

You will also have to pay all normal PayPal fees associated with their service.
