# High Level

StreamersEdge has a very simple payment and redemption from the user's perspective, but at the backend there are several moving parts,

The following diagram illustrates, at a high-level, from both a Streamer and a Viewers perspective the flow of money during an average StreamersEdge experience.

![](<../.gitbook/assets/SE Payment Flow - High Level (3).png>)

From the user's perspective there are only two StreamersEdge fees they need to pay:

1. For every donation a $0.02USD blockchain transaction fee is paid by the donator. The streamer pays no fee and receives the full amount of every donation on completion of any challenge.
2. Every time a user withdraws money from their StreamersEdge wallet and deposits it in their PayPal account there is a 10% fee. No fee is charged when a user funds their wallet from PayPal.

The fees charged to the user's are to pay the Peerplays blockchain fee which is always a set fee of $0.02USD, and payments to Peerplays Global, the operators of StreamersEdge.

The 10% fee that goes to Peerplays Global can then be further distributed to Peerplays token holders or remain in the Peerplays Global account. \[TBD] Not sure the distribution % yet.
