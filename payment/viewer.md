# Viewer

Viewers are typically users that want to enjoy the experience of watching streamers play, and compete against challenges. It isn't mandatory for viewers to make donations, but it's part of the eSports culture for this to happen as incentive to the streamers.

Many streamers are also viewers, enjoying watching other streamers attempt challenges.

All donations are conditional. If a challenge isn't won by the streamer then their bounty is refunded to all the donators.

In the example below a viewer, with $500 in their PayPal account, decides to donate $10 to a challenge that has a bounty of $50. Other users make donations as well so that in total $60 is raised.&#x20;

![](<../.gitbook/assets/Payment Flow - Viewer.png>)

As the bounty has been reached the challenge is started.&#x20;

The diagram covers the scenarios where the challenger wins and keeps all donations, or the challenger loses and all donations are refunded. Fees are payable as follows:

* Each user pays a fixed transaction fee of $0.02 for each donation.
* If the user decides to withdraw any money from their StreamersEdge wallet account and deposit in PayPal there is a 5% fee charged. There is no fee charged for funding a StreamersEdge wallet.

{% hint style="warning" %}
**Note**: These fees are ONLY for StreamersEdge. The user will be expected to pay all normal PayPal fees when either depositing or withdrawing money.
{% endhint %}
