# Streamer

Streamers are the only users that can profit from StreamersEdge.

If a streamer wins a challenge they'll receive all the donations provided that the original bounty is met.

In the example below a streamer, with no money in their wallet, creates a new challenge and sets a bounty of $50. Other users make donations and in total $60 is raised. This meets the original bounty set and so the challenge can start.

![](<../.gitbook/assets/Payment Flow - Streamer.png>)

If a challenge is lost all donations are refunded to the donators.

No fees are charged to the streamer for creating a challenge or receiving donations, however, a fee of 5% is charged to withdraw money from user's StreamersEdge wallet to PayPal.

{% hint style="warning" %}
**Note**: These fees are ONLY for StreamersEdge. The user will be expected to pay all normal PayPal fees when either depositing or withdrawing money.
{% endhint %}
